//Subpage Loading
// if (history.pushState && window.XMLHttpRequest) { alert('supported'); }

window.onload = function () {
  if (!history.pushState && !window.XMLHttpRequest) return;
  var projectItem = document.querySelectorAll(".project-item");
  for (var i = 0; i < projectItem.length; i++) {
    projectItem[i].addEventListener("click", ProjectClick);
  }
  window.addEventListener("popstate", ProjectLoad);
}

function ProjectClick(event){
  event.preventDefault();
  var url = event.currentTarget.getAttribute("href");
  var projectId = event.currentTarget.getAttribute("id");
  var title = event.currentTarget.getAttribute("title");
  window.history.pushState(projectId, "(" + title + ") | Esperança Design", url);
  window.document.title = title + " | Esperança Design";
  ProjectLoad();
}

function ProjectLoad() {
  var file = window.location.href+".html";
  var projectContent = new XMLHttpRequest();
  httpRequest.onreadystatechange = placeContent;
  projectContent.open( 'GET', url );
	projectContent.responseType = 'document';
	projectContent.send();
  function placeContent() {

  }
}
//---


//Convert Scroll
function onScroll(event) {
  // delta is +120 when scrolling up, -120 when scrolling down
  var delta = event.detail ? event.detail * (-120) : event.wheelDelta;
  // set own scrolling offset, take inverted sign from delta (scroll down should scroll right,
  // not left and vice versa
  var scrollOffset = 40 * (delta / -120);
  // Scroll it
  window.scrollBy(scrollOffset, 0);
  // Not sure if the following two are necessary, you may have to evaluate this
  event.preventDefault;
  event.stopPropagation;
}

// The not so funny part... fin the right event for every browser
var mousewheelevt=(/Firefox/i.test(navigator.userAgent)) ? "DOMMouseScroll" : "mousewheel";
if (document.attachEvent)
document.attachEvent("on"+mousewheelevt, onScroll);
else if (document.addEventListener)
document.addEventListener(mousewheelevt, onScroll, false);
//---


// Convert Keyboard navegation

Mousetrap.bind("pageup", pageUp);
Mousetrap.bind("pagedown", pageDown);
Mousetrap.bind("home", homeKey);
Mousetrap.bind("end", endKey);

function pageUp() {
  window.scroll({left: 0, behavior: "smooth"});
}

function pageDown() {
  window.scroll({left: 2000000, behavior: "smooth"});
}

function homeKey() {
  window.scroll({left: 0, behavior: "smooth"});
}

function endKey() {
  window.scroll({left: 2000000, behavior: "smooth"});
}

//---


//Revealer Animation
var windowssizeW = window.innerWidth;
var revealerpointW = windowssizeW * 0.20;
window.addEventListener("scroll", reveal, {passive: true});
reveal();

function reveal() {
  var revealersH = document.querySelectorAll(".revealerH");
  for (var i = 0; i < revealersH.length; i++) {
    var revealerleft = revealersH[i].getBoundingClientRect().left;
    var revealerright = revealersH[i].getBoundingClientRect().right;
    if (revealerleft < windowssizeW - revealerpointW) {
      revealersH[i].classList.add("active");
    } else {
      // revealersH[i].classList.remove("active");
    }
    if (revealerright < 0 + revealerpointW) {
      // revealersH[i].classList.remove("active");
    }
  }
}
//---


//Fade-out Body on reload
// document.querySelector("#link-home").addEventListener("click", body_fade);
// function body_fade() {
//   document.body.classList.add("fade-out");
//   event
//   event.preventDefault();
//   setTimeout(function () {window.location.replace("");}, 500);
// }
//---


//Hide Intro
document.querySelector("#intro-scroll").addEventListener("click", close_intro);
window.addEventListener("scroll", close_intro);

document.querySelector("#link-home").addEventListener("click", function() {
  window.sessionStorage.clear();
});

var siteHeader = document.querySelector(".site-header");
var introState = sessionStorage.getItem("intro_state");
if (introState == "closed") {close_intro();}

function close_intro() {
  siteHeader.classList.add("inactive");
  document.querySelector(".site-nav-wpr").classList.add("active");

  if (introState != "closed") {
    siteHeader.style.transition = "all 0s 0.9s";
    window.scroll({left: 0, behavior: "instant"});
    setTimeout(function() {
      window.scroll({left: 0, behavior: "smooth"});
      document.querySelector("body").classList.add("nav-active");
    }, 900);
  } else {
    document.querySelector("body").classList.add("nav-active");
    reveal();
  };

  setTimeout(function() {
    window.removeEventListener("scroll", close_intro);
    window.sessionStorage.setItem("intro_state", "closed");
    reveal();
  }, 910);
}
//---
